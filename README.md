# Opencv Stitching sample

## Dependencies
- OpenCV (4.2.0)

### How to build

```console
$ mkdir build && cd build
$ cmake ../
$ make
```

### How to run
```console
$ cd build
$ ./ocv_stitching_detailed --output res.png ../test/*.png
```
