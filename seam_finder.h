//
//  seam_finder.h
//  sphere
//
//  Created by Alessandro Caldini on 09/03/2020.
//
//  SeamFinder class is a modified version of the cv::detail::DpSeamFinder class from OpenCV library.
//  This is due to an out of bounds access to an array in the cv::detail::DpSeamFinder::ComputeCosts
//  function that occurs sometimes during the stitching process.
//  The modification in SeamFinder::ComputeCosts (seam_finder.cc:623-649) checks the indexes in
//  order to avoid this error.

#ifndef seam_finder_hpp
#define seam_finder_hpp

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#include <opencv2/stitching/detail/seam_finders.hpp>
#pragma clang diagnostic pop

namespace stitcher {
namespace core {

class SeamFinder : public cv::detail::SeamFinder {
 public:
  enum CostFunction { COLOR, COLOR_GRAD };

  SeamFinder(CostFunction costFunc = COLOR);

  CostFunction GetCostFunction() const { return costFunc_; }
  void SetCostFunction(CostFunction val) { costFunc_ = val; }

  virtual void find(const std::vector<cv::UMat> &src,
                    const std::vector<cv::Point> &corners,
                    std::vector<cv::UMat> &masks) override;

 private:
  enum ComponentState {
    FIRST = 1,
    SECOND = 2,
    INTERS = 4,
    INTERS_FIRST = INTERS | FIRST,
    INTERS_SECOND = INTERS | SECOND
  };
  void Process(const cv::Mat &image1, const cv::Mat &image2, cv::Point tl1,
               cv::Point tl2, cv::Mat *mask1, cv::Mat *mask2);

  void FindComponents();

  void FindEdges();

  void ResolveConflicts(const cv::Mat &image1, const cv::Mat &image2,
                        cv::Point tl1, cv::Point tl2, cv::Mat *mask1,
                        cv::Mat *mask2);

  void ComputeGradients(const cv::Mat &image1, const cv::Mat &image2);

  bool HasOnlyOneNeighbor(int comp);

  bool CloseToContour(int y, int x, const cv::Mat_<uchar> &contourMask);

  bool GetSeamTips(int comp1, int comp2, cv::Point *p1, cv::Point *p2);

  void ComputeCosts(const cv::Mat &image1, const cv::Mat &image2, cv::Point tl1,
                    cv::Point tl2, int comp, cv::Mat_<float> *costV,
                    cv::Mat_<float> *costH);

  bool EstimateSeam(const cv::Mat &image1, const cv::Mat &image2, cv::Point tl1,
                    cv::Point tl2, int comp, cv::Point p1, cv::Point p2,
                    std::vector<cv::Point> *seam, bool *isHorizontal);

  void UpdateLabelsUsingSeam(int comp1, int comp2,
                             const std::vector<cv::Point> &seam,
                             bool isHorizontalSeam);

  CostFunction costFunc_;

  // processing images pair data
  cv::Point union_tl_, union_br_;
  cv::Size union_size_;
  cv::Mat_<uchar> mask1_, mask2_;
  cv::Mat_<uchar> contour1mask_, contour2mask_;
  cv::Mat_<float> gradx1_, grady1_;
  cv::Mat_<float> gradx2_, grady2_;

  // components data
  int ncomps_;
  cv::Mat_<int> labels_;
  std::vector<ComponentState> states_;
  std::vector<cv::Point> tls_, brs_;
  std::vector<std::vector<cv::Point> > contours_;
  std::set<std::pair<int, int> > edges_;
};

}  // namespace core
}  // namespace stitcher

#endif /* seam_finder_h */
